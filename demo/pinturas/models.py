from django.db import models
from django.contrib.auth.models import User



# Create your models here.

class BrandQuerySet(models.QuerySet):
    def old(self):
        return self.filter(id__lt=3)


class BaseModel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    user_update = models.IntegerField(blank=True, null=True)


    class Meta:
        abstract = True


class Category(BaseModel):
    description = models.CharField(
        max_length = 128,
        help_text = "Category Description",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Category, self).save()

    class Meta:
        verbose_name_plural = "Categories"

class Era(BaseModel):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.CharField(
        max_length = 128,
        help_text = "Era Description"
    )

    def __str__(self):
        return "{0} - {1}".format(self.category.description, self.description)


    def save(self):
        self.description = self.description.upper()
        super(Era, self).save()

    class Meta:
        verbose_name_plural = "Eras"
        unique_together = ("category", "description")

class Museo(BaseModel):
    objects = BrandQuerySet.as_manager()
    description = models.CharField(
        max_length = 128,
        help_text = "Description Museo",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Museo, self).save()

    class Meta:
        verbose_name_plural = "Museos"
class Artista(BaseModel):
    description = models.CharField(
        max_length = 128,
        help_text = "Artist Description",
        unique = True
    )

    def __str__(self):
        return "{}".format(self.description)

    def save(self):
        self.description = self.description.upper()
        super(Artista, self).save()

    class Meta:
        verbose_name_plural = "Artistas"


##### PRODUCT MODEL #####

class Obra(BaseModel):
    code = models.CharField(max_length=16, unique=True)
    description = models.CharField(max_length=128)
    price = models.FloatField(default=0)
    last_buy = models.DateField(null=True, blank=True)
    museo = models.ForeignKey(Museo, on_delete=models.CASCADE)
    artista = models.ForeignKey(Artista, on_delete=models.CASCADE)
    era = models.ForeignKey(Era, on_delete=models.CASCADE)


    def __str__(self):
        return "{}".format(self.description)


    def save(self):
        self.description = self.description.upper()
        super(Obra, self).save()


    class Meta:
        verbose_name_plural = "Obras"
