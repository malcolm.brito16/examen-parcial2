from django.shortcuts import render
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import Articulo
from .forms import NewArticuloForm
# Create your views here.

##### CRUD ARTICULO #####

## Create
## generic.CreateView
class NewArticulo(LoginRequiredMixin, generic.CreateView):
    template_name = "articulos/new_articulo.html"
    model = Articulo
    context_object_name = "obj"
    form_class = NewArticuloForm
    login_url = "home:login"
    success_url = reverse_lazy("articulos:list_articulo")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


## Retrieve
# generic.ListView
class ListArticulo(LoginRequiredMixin, generic.ListView):
    template_name = "articulos/list_articulo.html"
    queryset = Articulo.objects.all()
    login_url = "home:login"
    context_object_name = "obj"

## Update

class UpdateArticulo(LoginRequiredMixin, generic.UpdateView):
    template_name = "articulos/new_articulo.html"
    model = Articulo
    context_object_name = "obj"
    form_class = NewArticuloForm
    login_url = "home:login"
    success_url = reverse_lazy("articulos:list_articulo")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)
## Delete
