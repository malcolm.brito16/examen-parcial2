from django.urls import path

from articulos import views

app_name = "articulos"

urlpatterns =[
    path('list_articulo/', views.ListArticulo.as_view(), name="list_articulo"),
    path('new_articulo/', views.NewArticulo.as_view(), name="new_articulo"),
]
