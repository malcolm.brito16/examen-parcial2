from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Client(models.Model):
    client = models.OneToOneField(User, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)
    level = models.IntegerField()
    status = models.BooleanField(default=True)

    def __str__(self):
        return "{}".format(self.client.username)
# Create your models here.
