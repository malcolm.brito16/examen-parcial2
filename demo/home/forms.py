from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Client

class ClientForm(UserCreationForm):
    username = forms.CharField(max_length=150, widget=forms.TextInput(attrs={"class":"form-control"}))
    password1 = forms.CharField(max_length=150, widget=forms.PasswordInput(attrs={"class":"form-control"}))
    password2 = forms.CharField(max_length=150, widget=forms.PasswordInput(attrs={"class":"form-control"}))
    class Meta:
        model = User
        fields = [
            "username",
            "password1",
            "password2"
        ]


    level = forms.IntegerField(widget=forms.NumberInput(attrs={"class":"form-control"}))
    status = forms.BooleanField()
class Users_Form(UserCreationForm):

	email = forms.CharField(max_length=48)
